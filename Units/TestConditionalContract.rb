
require "minitest/autorun"
require_relative "../UContract"

class GoodTestClassConditional

  # Short-cut
  contract.requires(:method, :first_fail).has_conditional_contracts do
    calls(:draw)
    calls(:refresh)
  end
  
  contract.requires(:class).has_conditional_contracts do
    implements(:draw)
    implements(:refresh)
  end
  
  # Short-cut + plain declaration
  contract.requires(:class).has_conditional_contracts do
    implements(:draw)
    contract.requires(:method, :draw).calls(:refresh)
  end

  def first_fail
    refresh
  end


  contract.requires(:method, :both_pass).has_conditional_contracts do
    calls(:draw)
    calls(:refresh)
  end

  def both_pass
    draw()
    refresh
  end

end

class BadTestClassConditional

  contract.requires(:method, :bad_meth).has_conditional_contracts do
    calls(:draw)
    calls(:refresh)
  end
  
  # Short-cut + plain declaration
  contract.requires(:class).has_conditional_contracts do
    implements(:draw)
    contract.requires(:method, :draw).calls(:refresh)
  end

  def bad_meth
    draw()
    #refresh
  end
  
  
  # Lack second contract
  contract.requires(:class).has_conditional_contracts do
    implements(:draw)
  end
  def draw
    # ...
  end
  
end

class TestConditionalContract < MiniTest::Test
  
  def test_good_testConditional
    for contract in GoodTestClassConditional.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_testConditional
    for contract in BadTestClassConditional.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
end