
require "minitest/autorun"
require_relative "../UContract"

class GoodImplementTestClass
  
  contract.requires(:class).implements(:draw)
  
  def draw
    
  end
  
  def reset
    
  end
  
  contract.requires(:class).implements(:reset)
  
end

class BadImplementTestClass
  
  contract.requires(:class).implements(:draw)
  
  def foo
    
  end
  
  contract.requires(:class).implements(:reset)
  
end

class TestImplement < MiniTest::Test
  
  def test_good_test_class
    for contract in GoodImplementTestClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_test_class
    for contract in BadImplementTestClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end
