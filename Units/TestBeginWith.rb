
require_relative "../UContract"
require "minitest/autorun"

class GoodTestClassBeginWith
  
  contract.suggests(:allMethods).begin_with_call(:super)
  def m
    super()
  end
  
  def if_m(a)
    if a == 1
      super(1)
    elsif a == 2
      super(2)
    else
      super(0)
    end
  end
  
  def case_m(a)
    case a
    when 1
      super(1)
    else
      super(0)
    end 
  end
  
  def while_m()
    while 1
      super(1)
    end
    super(2)
  end
  
end

class BadTestClassBeginWith
  
  contract.requires(:allMethods).begin_with_call(:super)
  def m
    #super()
  end
  
  def if_m(a)
    if a == 1
      #super(1)
    elsif a == 2
      super(2)
    else
      super(0)
    end
  end
  
  def case_m(a)
    case a
    when 1
      #super(1)
    else
      super(0)
    end 
  end
  
  def while_m()
    while 1
      #super(1)
    end
  end
  
end

class TestBeginWith < MiniTest::Test
  
  def test_good_me13
    for contract in GoodTestClassBeginWith.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_me13
    for contract in BadTestClassBeginWith.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end
