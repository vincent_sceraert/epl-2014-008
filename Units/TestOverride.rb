
require "minitest/autorun"
require_relative "../UContract"

class TestOverrideClass 
  
  contract.requires(:subclasses).override(:foo)
  def foo
    
  end
  
  contract.requires(:subclasses).override(:foo2)
  def foo2
    
  end
  
  contract.requires(:subclasses).override(:foo3)
  def foo3
    
  end
  
end

class TestOverrideBadChildClass < TestOverrideClass
  
  def foo2

  end
  
  def foo3
    #comment
  end
end

class TestOverrideGoodChildClass < TestOverrideClass
  
  def foo
    puts 1
  end
  
  def foo2
    super
  end
  
  def foo3
    #comment
    puts 2
  end
end

class TestOverride < MiniTest::Test
  def test_BadChildOverride
    for contract in TestOverrideBadChildClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
  def test_GoodChildOverride
    for contract in TestOverrideGoodChildClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
end
