
require "minitest/autorun"
require_relative "../UContract"

class Utils
  
  def eq(first, second)
    return first == second
  end
  
end

class GoodCallTestStaticCallClass
  contract.requires('allMethods').static_call(:Utils, :eq)
  def good_meth
    p "I'm good"
    Utils.eq(1, 2)
  end
  
  def good_if_meth(a)
    if a == 1
      puts "1"
      Utils.eq(1, 2)
    else
      puts "2"
      Utils.eq(2, 1)
    end
  end
  
  def good_case_meth(a)
    case a
      when 1
        puts 1
        Utils.eq(2, 1)
      when 2
         puts 2
         Utils.eq(2, 3)
      else
         Utils.eq(0, 1)
         puts 0
     end
  end 
end

class BadCallTestStaticCallClass
  contract.requires(:allMethods).static_call(:Utils, :eq)
  def bad_meth
    puts "I'm bad"
  end
  
  def bad_if_meth(a)
    if a == 1
      puts "No static call"
    else
      Utils.eq(2, 1)
    end
  end
  
  def bad_case_meth(a)
    case a
      when 1
        puts 1
        Utils.eq(2, 1)
      when 2
         puts "No static call"
      else
         Utils.eq(2, 1)
         puts 0
     end
  end 
end

class TestStaticCall < MiniTest::Test

  def test_good_testStaticCall_class
    for contract in GoodCallTestStaticCallClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_testStaticCall_class
    for contract in BadCallTestStaticCallClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end
