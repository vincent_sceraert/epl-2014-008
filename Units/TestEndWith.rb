
require "minitest/autorun"
require_relative "../UContract"

class GoodTestClassEndWith
  
  contract.requires(:allMethods).end_with_call(:return)
  def m
    return "m"
  end
  
  def if_m(a)
    if a == 1
      return 1 + 1
    else
      return 1 + 2
    end
  end
  
  def case_m(a)
    case a
    when 1
      return 1 + 1
    when 2
      return 1 + 2
    else
      return 1
    end
  end
  
end

class BadTestClassEndWith
  
  contract.requires(:allMethods).end_with_call(:return)
  def m
    puts "m"
  end
  
  def if_m(a)
    if a == 1
      return 1 + 1
    else
      #return 1 + 2
      puts "1 + 2"
    end
  end
  
  def case_m(a)
    case a
    when 1
      return 1 + 1
    when 2
      #return 1 + 2
      puts "1 + 2"
    else
      return 1
    end
  end
  
end

class TestEndWith < MiniTest::Test
  
  def test_good_EndWith_testclass
    for contract in GoodTestClassEndWith.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_EndWith_testclass
    for contract in BadTestClassEndWith.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
end