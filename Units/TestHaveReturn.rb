
require "minitest/autorun"
require_relative "../UContract"

class GoodCallTestHaveReturnClass
  contract.requires(:allMethods).have_return
  def good_meth
    return 1
  end
  
  def good_if_meth(a)
    if a == 1
      puts "1"
      return 1
    else
      puts "2"
      return 2
    end
  end
  
  def good_case_meth(a)
    case a
      when 1
        puts 1
        return 1
      when 2
         puts 2
         return 2
      else
         puts 0
         return 0
     end
  end 
end

class BadCallTestHaveReturnClass
  contract.requires("allMethods").have_return
  def bad_meth
    puts "I'm bad"
  end
  
  def bad_if_meth(a)
    if a == 1
      puts "No return"
    else
      return 2
    end
  end
  
  def bad_case_meth(a)
    case a
      when 1
        puts 1
        super(1)
        return 1
      when 2
         puts "No return"
      else
         super(0)
         puts 0
         return 0
     end
  end 
end

class TestHaveReturn < MiniTest::Test

  def test_good_test_class
    for contract in GoodCallTestHaveReturnClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_test_class
    for contract in BadCallTestHaveReturnClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end
