
require "minitest/autorun"
require_relative "../UContract"

class TestDisjunctionalContractGoodClass
  
  contract.requires(:method, :super_draw).has_disjunctional_contracts do
    calls(:refresh)
    calls(:super)
  end
  def super_draw
    super
  end
  
  contract.requires(:method, :refresh_draw).has_disjunctional_contracts do
    calls(:refresh)
    calls(:super)
  end
  def refresh_draw
    refresh
  end
  
  contract.requires(:method, :puts_draw).has_disjunctional_contracts do
    calls(:refresh)
    calls(:puts)
    calls(:super)
  end
  def puts_draw
    puts "draw"
  end
end
  
class TestDisjunctionalContractBadClass
  
  contract.requires(:method, :bad_draw).has_disjunctional_contracts do
    calls(:refresh)
    calls(:super)
  end
  def bad_draw
    puts "bad"
  end
end

class TestDisjunctionalContract < MiniTest::Test
  
  def test_good_testConditional
    for contract in TestDisjunctionalContractGoodClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_testConditional
    for contract in TestDisjunctionalContractBadClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
end
