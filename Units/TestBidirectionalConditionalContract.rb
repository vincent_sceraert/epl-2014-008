

require "minitest/autorun"
require_relative "../UContract"

class TestClassBiConditional

  contract.requires(:allMethods).has_biconditional_contracts do
    call(:draw)
    call(:refresh)
  end
  
  def call_draw
    draw()
  end
  
  def call_refresh
    refresh()
  end
  
  def call_both
    draw()
    refresh()
  end
  
  
end

class TestConditionalContract < MiniTest::Test
  
  def test_both_testBiConditional
    for contract in TestClassBiConditional.get_contracts
      puts contract.method_name
      if contract.method_name == :call_both
        assert_equal(:pass, contract.result)
      end
    end
  end
  
  def test_refresh_testBiConditional
    for contract in TestClassBiConditional.get_contracts
      puts contract.method_name
      if contract.method_name == :call_refresh
        assert_equal(:fail, contract.result)
      end
    end
  end
  
  def test_draw_testBiConditional
    for contract in TestClassBiConditional.get_contracts
      puts contract.method_name
      if contract.method_name == :call_draw
        assert_equal(:fail, contract.result)
      end
    end
  end
  
end