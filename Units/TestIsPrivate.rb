
require "minitest/autorun"
require_relative "../UContract"

class TestIsPrivate < MiniTest::Test
  
  class TestIsPrivateClass
    private
    def pr
      
    end
    
    def self.spr
      
    end
    
    def self.pspr
      
    end
    private_class_method :pspr
    
    public
    def pu
      
    end
    
    def self.spu
      
    end
    
    protected
    def po
      
    end
    
    def self.spo
      
    end
    

  end

  def test_private_method
    c = TestIsPrivateClass.contract.requires(:method, :pr).is_private
    assert_equal(:pass, c.result)
  end
  
  def test_static_private_method
    c = TestIsPrivateClass.contract.requires(:method, :spr).is_private
    assert_equal(:fail, c.result)
  end

  def test_private_static_private_method
    c = TestIsPrivateClass.contract.requires(:method, :pspr).is_private
    assert_equal(:pass, c.result)
  end

  def test_public_method
    c = TestIsPrivateClass.contract.requires(:method, :pu).is_private
    assert_equal(:fail, c.result)
  end
  
  def test_static_public_method
    c = TestIsPrivateClass.contract.requires(:method, :spu).is_private
    assert_equal(:fail, c.result)
  end
  
  def test_protected_method
    c = TestIsPrivateClass.contract.requires(:method, :po).is_private
    assert_equal(:fail, c.result)
  end
  
  def test_static_protected_method
    c = TestIsPrivateClass.contract.requires(:method, :spo).is_private
    assert_equal(:fail, c.result)
  end

end
  
  
