
require "minitest/autorun"
require_relative "../UContract"


module TestIncludeExtendModuleM
  
end

module TestIncludeExtendModuleMExtend

end

class TestIncludeExtendModuleGoodClass
  include TestIncludeExtendModuleM
  extend TestIncludeExtendModuleMExtend
end

class TestIncludeExtendModuleBadClass
  
end

class TestIncludeExtendModule < MiniTest::Test
  
  def test_include_module_good_include_m
    assert_equal(:pass, TestIncludeExtendModuleGoodClass.contract.requires(:class).include_module(:TestIncludeExtendModuleM).result)
  end
  
  def test_include_module_bad_include_m
    assert_equal(:fail, TestIncludeExtendModuleBadClass.contract.requires(:class).include_module(:TestIncludeExtendModuleM).result)
  end

  def test_include_module_good_extend_m
    assert_equal(:fail, TestIncludeExtendModuleGoodClass.contract.requires(:class).extend_module(:TestIncludeExtendModuleM).result)
  end

   def test_include_module_bad_extend_m
    assert_equal(:fail, TestIncludeExtendModuleBadClass.contract.requires(:class).extend_module(:TestIncludeExtendModuleM).result)
  end
  
  def test_include_module_good_include_me
    assert_equal(:fail, TestIncludeExtendModuleGoodClass.contract.requires(:class).include_module(:TestIncludeExtendModuleMExtend).result)
  end
  
  def test_include_module_bad_include_me
    assert_equal(:fail, TestIncludeExtendModuleBadClass.contract.requires(:class).include_module(:TestIncludeExtendModuleMExtend).result)
  end

  def test_include_module_good_extend_me
    assert_equal(:pass, TestIncludeExtendModuleGoodClass.contract.requires(:class).extend_module(:TestIncludeExtendModuleMExtend).result)
  end

   def test_include_module_bad_extend_me
    assert_equal(:fail, TestIncludeExtendModuleBadClass.contract.requires(:class).extend_module(:TestIncludeExtendModuleMExtend).result)
  end

end