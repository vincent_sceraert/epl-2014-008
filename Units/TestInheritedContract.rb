
require "minitest/autorun"
require_relative "../UContract"

class TestInheritance
  contract.requires(:methodsOverriding, :fct_1).calls(:puts)
  
  contract.requires(:methodAndOverriders, :fct_2).calls(:puts)
  def fct_2
    puts 2
  end
  
  contract.requires(:subclasses).implements(:fct_3)
  contract.requires(:directSubclasses).implements(:fct_4)
  
end

class GoodTChild < TestInheritance
  
  def fct_1
    puts 1
  end
  
  def fct_2
    puts "First a good puts"
    super()
  end
  
  def fct_3
    puts "fct_3"
  end
  
  def fct_4
    puts "fct_3"
  end
  
end

class TGrandChild < GoodTChild
    
end

class TestInheritedContract < MiniTest::Test
  
  def test_T
    for contract in TestInheritance.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_GoodTChild
    for contract in GoodTChild.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_TGrandChild
    for contract in TGrandChild.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  
end