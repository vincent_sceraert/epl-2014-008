
require "minitest/autorun"
require_relative "../UContract"

class GoodCallTestAssignClass
  contract.requires('allMethods').assigns(:@a)
  def good_meth
    puts "I'm good"
    @a = 5
  end
  
  contract.requires(:method, :good_if_meth).assigns(:@@a)
  def good_if_meth(a)
    if a == 1
      puts "1"
      @a = 1
      @@a = 11
    else
      puts "2"
      @a = 2
      @@a = 22
    end
  end
  
  def good_case_meth(a)
    case a
      when 1
        puts 1
        @a = 1
      when 2
         puts 2
         @a = 2
      else
         super(0)
         @a = 0
     end
  end 
end

class BadCallTestAssignClass
  contract.requires(:method, :bad_meth).assigns(:@a)
  def bad_meth
    puts "I'm bad"
  end
  
  contract.requires(:method, :bad_if_meth).assigns(:@a)
  def bad_if_meth(a)
    if a == 1
      puts "No assign"
    else
      @a = 2
    end
  end
  
  contract.requires(:method, :bad_case_meth).assigns(:@a)
  def bad_case_meth(a)
    case a
      when 1
        puts 1
        @a = 1
      when 2
         puts "No assign"
      else
         super(0)
         @a = 0
     end
  end 
end

class TestAssign < MiniTest::Test

  def test_good_testAssign_class
    for contract in GoodCallTestAssignClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_testAssign_class
    for contract in BadCallTestAssignClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end

