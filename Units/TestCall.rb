
require "minitest/autorun"
require_relative "../UContract"

class GoodCallTestClass
  contract.requires('allMethods').call("super")
  def good_meth
    p "I'm good"
    super()
  end
  
  contract.requires(:method, :good_if_meth).calls(:puts)
  def good_if_meth(a)
    if a == 1
      puts "1"
      super(1)
    else
      puts "2"
      super(2)
    end
  end
  
  def good_case_meth(a)
    case a
      when 1
        puts 1
        super(1)
      when 2
         puts 2
         super(2)
      else
         super(0)
         puts 0
     end
  end 
end

class BadCallTestClass
  contract.requires(:method, :bad_meth).calls(:super)
  def bad_meth
    puts "I'm good"
  end
  
  contract.requires(:method, :bad_if_meth).calls(:puts)
  def bad_if_meth(a)
    if a == 1
      puts "No super"
    else
      super(2)
    end
  end
  
  contract.requires(:method, :bad_case_meth).calls(:super)
  def bad_case_meth(a)
    case a
      when 1
        puts 1
        super(1)
      when 2
         puts "No super"
      else
         super(0)
         puts 0
     end
  end 
end

class TestCall < MiniTest::Test

  def test_good_test_class
    for contract in GoodCallTestClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_test_class
    for contract in BadCallTestClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end
