
require "minitest/autorun"
require_relative "../UContract"

class GoodTestPGClass
  
  contract.requires(:class).match_table(:contract, :foo)
  
end

class BadTestPGClass
  
  contract.requires(:class).matches_table(:thesis)
  
end

class TestPG < MiniTest::Test

  def test_good_testPG_class
    for contract in GoodTestPGClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_bad_testPG_class
    for contract in BadTestPGClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
  
end

