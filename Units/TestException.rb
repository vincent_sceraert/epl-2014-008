
require "minitest/autorun"
require_relative "../UContract"

class TestExceptionClass
   contract.requires(:allMethods).end_with_call(:return).exception(:methods, :called, :initialize)
                                                        .exception(:methods, :nameBeginWith, :f)
                                                        .exception(:methods, :nameEndWith, :test)
                                                        .exception(:methods, :nameContainWord, :tmp)
                                                        .exception(:methods, :nameDoNotContainWord, :return)
   contract.requires(:methodsOverriding, :foo).call(:super).exception(:classes, :nameContainWord, :Test)

   

   def initialize
     puts "Shouldn't be taken in account"
   end
   
   def tmp_?
     puts "Shouldn't be taken in account"
   end
   
   def f_test
     puts "Shouldn't be taken in account"
   end
   
   def foo
     puts "Shouldn't be taken in account"
     return 1
   end
   
   def return_meth
     return 1
   end
   
end

class TestExceptionClassChild < TestExceptionClass
  def foo
    puts "Shouldn't be taken in account"
  end
end

class ChildExceptionClass < TestExceptionClass
  def foo
    puts "Should be taken in account"
  end
end


class TestException < MiniTest::Test
  def test_test_exception_class
    for contract in TestExceptionClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_test_exception_class_child
    for contract in TestExceptionClassChild.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
  def test_child_exception_class
    for contract in ChildExceptionClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
end