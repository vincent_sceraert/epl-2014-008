
require "minitest/autorun"
require_relative "../UContract"

class TestClassMethodLength
  
  contract.requires(:allMethods).maxLength(5)
  def foo
    puts 1
  end
  
  def long_foo
=begin
aaaaaaaaaaaaaaa

a  
=end

  puts "=end"

=begin
=end
    
    
    
    
    
    # puts
    puts 1
  end
  
  # Not tested because static
  def self.self_foo
    puts "self"
  end
end


class TestMaxLength < MiniTest::Test
  
  def test_max_length
    for contract in TestClassMethodLength.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
end