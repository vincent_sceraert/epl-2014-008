
require "minitest/autorun"
require_relative "../UContract"

class TestCustomContractClass
   
  contract.requires(:method, :meth).respect_custom_contract do
   |args, ucontract|
   mcs = MethodCallSeeker.new(ucontract.class_name, ucontract.method_name)
   next mcs.apply(:super)
  end
  def meth
    super
  end

end

class TestCustomContract < MiniTest::Test
  
  def test_custom_contract
    for contract in TestCustomContractClass.get_contracts
      assert_equal(:pass, contract.result)
    end
  end
  
end
