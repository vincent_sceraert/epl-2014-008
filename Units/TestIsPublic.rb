
require "minitest/autorun"
require_relative "../UContract"

class TestIsPublic < MiniTest::Test
  
  class TestIsPublicClass
    private
    def pr
      
    end
    
    def self.spr
      
    end
    
    def self.pspr
      
    end
    private_class_method :pspr
    
    public
    def pu
      
    end
    
    def self.spu
      
    end
    
    protected
    def po
      
    end
    
    def self.spo
      
    end

  end

  def test_private_method
    c = TestIsPublicClass.contract.requires(:method, :pr).is_public
    assert_equal(:fail, c.result)
  end
  
  def test_static_private_method
    c = TestIsPublicClass.contract.requires(:method, :spr).is_public
    assert_equal(:pass, c.result)
  end

  def test_private_static_private_method
    c = TestIsPublicClass.contract.requires(:method, :pspr).is_public
    assert_equal(:fail, c.result)
  end

  def test_public_method
    c = TestIsPublicClass.contract.requires(:method, :pu).is_public
    assert_equal(:pass, c.result)
  end
  
  def test_static_public_method
    c = TestIsPublicClass.contract.requires(:method, :spu).is_public
    assert_equal(:pass, c.result)
  end
  
  def test_protected_method
    c = TestIsPublicClass.contract.requires(:method, :po).is_public
    assert_equal(:fail, c.result)
  end
  
  def test_static_protected_method
    c = TestIsPublicClass.contract.requires(:method, :spo).is_public
    assert_equal(:pass, c.result)
  end

end
  
  
