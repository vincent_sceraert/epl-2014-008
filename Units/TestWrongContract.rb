
require "minitest/autorun"
require_relative "../UContract"


class WrongContractClass
  
  # Missing requires or suggests
  contract.implements(:test)
  # Wrong target
  contract.requires(:metho, :foo).begin_with_call(:super)
  # Wrong contract name
  contract.requires(:method, :meth).wrong
  # Wrong exception
  # First bad target then bad rule
  contract.requires(:method, :meth).calls(:super).exception(:bad, :nameContainWord, :Test) 
                                                 .exception(:classes, :bad, :Test)
  def meth
    
  end
end


class TestWrongContract < MiniTest::Test
  
  def test_wrong_contracts
    for contract in WrongContractClass.get_contracts
      assert_equal(:fail, contract.result)
    end
  end
end
