
require_relative "./UContract"
require_relative "./CustomClasses/String"

contracts = []
=begin
  Class which test the tool itself with usage contracts
  Used to check if the modifications of the tool (new Contract types, ...) respects implicit constraints of the tool  
=end
class TestTool
  
end

###############################################################################
######### Contracts folder
###############################################################################

# Require all contracts
contracts_name = []
Dir['./Contracts/*.rb'].each {
  |file| 
  require file 
  # Retrieve name of file without extension
  name = file.split("/").last.split(".")[0]
  
  # Check if class with file name exists and add it to the list of contracts if it matches
  TestTool.contract.requires(:class).respects_custom_contract do
    inside = false
    ObjectSpace.each_object(Class) {|cl| inside = true if name == cl.to_s }
    if inside
      contracts_name.push(name) if !contracts_name.include?(name)
    else
      puts "#{name.to_s} class not found in file #{name.to_s}.rb".red
    end
    next inside
  end
}

contracts_calls = Hash.new
# Apply verification on Contract classes
contracts_name.each{
  |c_name|
  class_inst = Object.const_get(c_name)
  
  # If apply implemented, the contract is callable so @call_name must be assign
  class_inst.contract.requires(:class).has_conditional_contracts do
    implements(:apply)
    assigns(:@call_names)
  end
  
   # If apply implemented, the method must have a return
  class_inst.contract.requires(:class).has_conditional_contracts do
    implements(:apply)
    contract.requires(:method, :apply).calls(:return)
  end
  
  # Two contracts can't use the same name to be called
  class_inst.contract.requires(:class).respects_custom_contract contracts_calls do
    |args, ucontract|
    c_calls = args[0]
    #puts "name : " + ucontract.class_name
    result = true
    if class_inst.call_names != nil
      class_inst.call_names.each{
         |command_name|
         if c_calls.keys.include?(command_name.to_s) and c_calls[command_name] != ucontract.class_name
           puts command_name.to_s + " is already used by contract " + c_calls[command_name]
           result = false
         else
           c_calls[command_name.to_s] = c_name.to_s
         end
      }
      next result
    end
    next true
  end

  # Check if class is child of Contract
  class_inst.contract.requires(:class).respects_custom_contract do
    parent = class_inst.superclass
    while parent != Contract and parent != BasicObject
      parent = parent.superclass
    end
    puts "#{c_name} is not a child of Contract class".red if parent != Contract and class_inst != Contract
    next (parent == Contract or class_inst == Contract)
  end
  # Add contracts to list
  class_inst.get_contracts.each{
    |c|
    contracts.push(c)
  }
  
}


###############################################################################
######### InstructionUtils folder
###############################################################################

# Require all classes
contracts_name = []
Dir['./InstructionUtils/*.rb'].each {
  |file| 
  require file 
  # Retrieve name of file without extension
  name = file.split("/").last.split(".")[0]
  
  # Check if class with file name exists and add it to the list of contracts if it matches
  TestTool.contract.requires(:class).respects_custom_contract do
    inside = false
    ObjectSpace.each_object(Class) {|cl| inside = true if name == cl.to_s }
    ObjectSpace.each_object(Module) {|cl| inside = true if name == cl.to_s }
    if inside
      contracts_name.push(name) if !contracts_name.include?(name)
    else
      puts "#{name.to_s} class not found in file #{name.to_s}.rb".red
    end
    next inside
  end
}

# Apply verification on classes
contracts_name.each{
  |c_name|
  class_inst = Object.const_get(c_name)
  
  # The class must implement the method check if its name ends with Checker
  class_inst.contract.requires(:class).implements(:check) if c_name.end_with?("Checker") and c_name != "InstructionChecker"
  
  # The class must implement the method get_instructions or include the module InstructionSeeker if its name ends with Seeker
  if c_name.end_with?("Seeker") and c_name != "InstructionSeeker"
    class_inst.contract.requires(:class).has_disjunctional_contracts do
      implements(:get_instructions) 
      includes_module(:InstructionSeeker) 
    end
  end
  
  # Add contracts to list
  class_inst.get_contracts.each{
    |c|
    contracts.push(c)
  }
  
}

TestTool.get_contracts.each{
  |c|
  contracts.push(c)
}
###############################################################################
######### Output formatter
###############################################################################

# Distinguish contracts by result
pass = []
fail = []
contracts.each{
  |ucontract|
  if ucontract.result == :pass
    pass.push(ucontract)
  else
    fail.push(ucontract)
  end
}

UContractHTML.show_contracts(sort_by: :class)
#UContractPDF.show_contracts(sortBy: :class)

