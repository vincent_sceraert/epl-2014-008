=begin
  Parser which retrieves source code of a method and returns its parse tree 
=end
require_relative "source_code_getter/method_source"
require_relative "../CustomClasses/String"

require "ripper"

class Parser

=begin
    Parse a method from a given class
    Will itself determine if static or not and 
    use the appropriate function
=end
  def self.parse(class_name, method_name)
    # Create a new object with the given String
    class_inst = Object.const_get(class_name)
    
    # Check if method is static or not
    if class_inst.method_defined?(method_name) || method_name == "initialize"
      return self.parse_non_static(class_inst, method_name)
    elsif class_inst.respond_to?(method_name)
      return self.parse_static(class_inst, method_name)
    else  
     return [0, [Array.new]]
    end
  end
  
  def self.parse_non_static(class_inst, method_name)
    return Ripper.sexp(class_inst.instance_method(method_name).source)
  end
  
  def self.parse_static(class_inst, method_name)
    return Ripper.sexp(class_inst.method(method_name).source)
  end

=begin
  From a parse tree obtained by the previous functions,
  the method will retrieve the element which begins with
  "bodystmt" which corresponds to the body of the method and return it
=end
  def self.get_commands(parse_tree)
    for element in parse_tree[1][0]
      if element[0] == :bodystmt
        return element[1]
      end
    end
    return [[:void_stmt]]
  end
  
  def self.get_source_code(class_obj, method_name)
    if class_obj.method_defined?(method_name) || method_name == "initialize"
      return class_obj.instance_method(method_name).source
    elsif class_obj.respond_to?(method_name)
      return class_obj.method(method_name).source
    else  
     return ""
    end
  end
  
end
