=begin
  Custom String declaration with new coloring method 
=end
class String

	def color(color)
		return "\e[#{color}m#{self}\e[0m"
	end

	def red
		return color(31)
	end


	def green
		return color(32)
	end

	def yellow
		return color(33)
	end

end
