=begin
  Custom Module declaration with new methods  
=end
class Module

###############################################################################
###############  VARIABLES
###############################################################################  

  @contracts = Array.new
  def contracts
    return @contracts
  end
  def contracts=(new_contracts)
    @contracts = new_contracts.dup
  end

  @children_contracts = Array.new
  def children_contracts
    return @children_contracts
  end
  def children_contracts=(new_contracts)
    @children_contracts = new_contracts.dup
  end
  
###############################################################################
###############  PUBLIC METHODS
###############################################################################  
=begin
  Add the contract to the list of contracts  
=end
  def add_contract(ucontract, contract, args, block)
    elem = [ucontract, contract, args, block]
    contracts.push(elem) if !contracts.include?(elem)
  end

###############################################################################
###############  DSL METHODS
###############################################################################
=begin
  Creates a new blank UContract and returns it 
=end
  def contract
    return UContract.new(self.to_s)
  end
  
=begin
  Shows in the console all contract applied on the class 
=end
  def show_contracts
    puts "Contracts of #{self.to_s}".green
    for contract in get_contracts
      puts "#{contract.to_s}\n"
    end
  end
  
=begin
  Return all contract applied on the class 
=end
  def get_contracts
    relevant_contracts = Array.new
    self.contracts = Array.new if self.contracts == nil
    for contract in self.contracts
      ucontract = contract[0]
      relevant_contracts.push(ucontract) if ucontract.method_name != "." and ucontract.method_name != "" and ucontract.timestamp != nil
    end
    return relevant_contracts
  end
  
=begin
  Method called when a method is compiled and added to the class
=end
  def method_added(method)
    # If first class in hierarchy
    if self.children_contracts == nil 
      self.contracts= Array.new()
      self.children_contracts= Array.new()
    end
    
    for contract in self.contracts
      self_contract = contract[0] # UContract instance
      self_check = contract[1] # name of the method to apply
      self_args = contract[2] # args of the method
      self_block = contract[3] # block of the contract
      case self_contract.method_name
      when method, "@" # contract on a method or a class
        self_contract.apply_contract(self_check, self_args, self_block)
      when "." # contract on all methods
        # Copy and modify contract to apply it on the method
        new_c = self_contract.copy
        new_c.method_name = method
        add_contract(new_c, self_check, self_args, self_block)
        new_c.apply_contract(self_check, self_args, self_block)
      end
    end
  end
  
  # Alias other hook method for static method added
  alias_method :singleton_method_added, :method_added
 
=begin
  Method called when the module is included by a module or class 
=end 
 def included(subclass)
     # If first class in hierarchy
    if self.contracts == nil 
      self.contracts = Array.new()
      self.children_contracts= Array.new()
    end  

    if subclass.class == Class or subclass.class == Module
      subclass.contracts = Array.new

      for contract in self.contracts
        parent_contract = contract[0] # UContract instance
        parent_check = contract[1] # name of the method to apply
        parent_args = contract[2] # args of the method
        parent_block = contract[3] # block of the contract
        new_contract = parent_contract.copy
        new_contract.class_name = subclass.to_s
        parent_target = parent_contract.target
        new_contract.target = :self if parent_target == :directChildren
        new_contract.target = :hierarchy if parent_target == :children
        new_contract_a = [new_contract, parent_check, parent_args, parent_block]
        subclass.add_contract(new_contract, parent_check, parent_args, parent_block)
        new_contract.apply_contract(parent_check, parent_args, parent_block)
      end
      subclass.children_contracts = Array.new
      for contract in subclass.contracts
        subclass.children_contracts.push(contract) if contract[0].target != :self 
      end
    end
  
    add_atexit(subclass)
    
  end
  
  def add_atexit(subclass)
    # Add callback to execute class contract when module is compiled
    class << subclass
      at_exit {
        @name = get_class(self.to_s)  
        # Check if class exists 
        exists = false   
        ObjectSpace.each_object(Class) { |cl| exists = true if cl.to_s == @name }
        ObjectSpace.each_object(Module) { |cl| exists = true if cl.to_s == @name }
        if exists
          # Rexecute all class contracts
          class_inst = Object.const_get(@name.to_s)
          for contract in class_inst.contracts
            if contract[0].method_name == "@"
              contract[0].apply_contract(contract[1], contract[2], contract[3])
            end
          end
        end
      }
    end
  end
  
  def get_class(name)
    name_s = name.to_s
    return (name_s.start_with?("#<Class:") and name_s.end_with?(">")) ? 
           get_class(name_s[8..name_s.length-2]) : name_s
  end
  
  # alias method for extended when a module is extended
  alias_method :extended, :included
  
end
