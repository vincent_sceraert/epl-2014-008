=begin
 Custom Class declaration with new methods
=end
require_relative "../UContract"
require_relative "./Module"

class Class
  
###############################################################################
###############  HOOK METHODS
###############################################################################
=begin
  Method called when the class is inherited by a subclass 
=end 
  def inherited(subclass)
    # If first class in hierarchy
    if self.children_contracts == nil 
      self.contracts = Array.new()
      self.children_contracts= Array.new()
    end   

    if subclass.class == Class
      subclass.contracts = Array.new
      for contract in self.children_contracts
        parent_contract = contract[0] # UContract instance
        parent_check = contract[1] # name of the method to apply
        parent_args = contract[2] # args of the method
        parent_block = contract[3] # block of the contract
        new_contract = parent_contract.copy
        new_contract.class_name = subclass.to_s
        parent_target = parent_contract.target
        new_contract.target = :self if parent_target == :directChildren
        new_contract.target = :hierarchy if parent_target == :children
        new_contract_a = [new_contract, parent_check, parent_args, parent_block]
        subclass.add_contract(new_contract, parent_check, parent_args, parent_block)
        new_contract.apply_contract(parent_check, parent_args, parent_block)
      end
      subclass.children_contracts = Array.new
      for contract in subclass.contracts
        subclass.children_contracts.push(contract) if contract[0].target != :self 
      end
    end
 
    add_atexit(subclass)
 
  end
end
