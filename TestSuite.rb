require 'simplecov'
=begin
  Loads all unit tests from Units folder and display the test coverage  
=end

# Exclude Units Tests and method_source gem from coverage
SimpleCov.start do
  add_filter "Units/"
  add_filter "Parser/source_code_getter"
end

require_relative "./UContract"

# Execute unit tests
Dir['./Units/*.rb'].each {|file| require file }
# Show unit tests in console
ObjectSpace.each_object(Class) {|cl| cl.show_contracts}
# Show unit tests in html page
UContractHTML.show_contracts(sort_by: :class)
UContractPDF.show_contracts(sort_by: :class)

# Open result of coverage verification and display it
system("explorer file:///#{File.dirname(__FILE__)}/coverage/index.html")

