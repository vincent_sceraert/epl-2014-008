
require 'pdfkit'
require_relative './HTML'
=begin
  Class which allow us to create a pdf document from a list of contracts for easy visualization  
=end
class UContractPDF
  
  # config/initializers/pdfkit.rb
  PDFKit.configure do |config|
    config.wkhtmltopdf = 'D:\wkhtmltopdf\bin\wkhtmltopdf.exe'
    config.default_options = {
      :page_size => 'Legal',
      :print_media_type => true
    }
    # Use only if your external hostname is unavailable on the server.
    config.root_url = "http://localhost"
    config.verbose = false
  end

  
  def self.show_contracts(classes: :all, contracts: :all, results: :all, types: :all, sort_by: :class)
    contracts_list = UContractHTML.get_list_contracts(classes, contracts, results, types, sort_by)
    create_pdf(contracts_list)
  end
=begin  
  def create_pdf(filename, dir)
    html = CodeRay.scan_file(filename).div
    name = filename.split("/")
    name = name[name.length - 1].split('.')[0]
    kit = PDFKit.new(html, :page_size => 'Letter')
    file = kit.to_file("./pdf/#{dir}/#{name}.pdf")
  end
=end  

  def self.create_pdf(contracts)
    UContractHTML.generate(contracts)
    file = File.open("./result/result.html", "r")
    html = file.read
    file.close
    #html = CodeRay.scan('./result/result.html')
    name = 'result.pdf'
    kit = PDFKit.new(html, :page_size => 'Letter')
    file = kit.to_file("./result/#{name}")
    system("explorer file:///#{File.dirname(__FILE__)}/result/result.pdf")
  end
  

end
