=begin
  Contract checking that if a first contract passes, the second must pass
=end
require_relative "./CompositeContract"

class ConditionalContract < CompositeContract
  
  @call_names = ["has_conditional_contracts", "have_conditional_contracts"]
  def apply(args, block)
    
    contracts = get_contracts(block)
    
    if contracts.length != 2
      puts "There as an error with the contract \n#{block.source}".red 
      return false
    end
    
    return true if !apply_contract(contracts[0])
    return false if !apply_contract(contracts[1])
    return true
  end
  
  def get_contracts(block)
    instructions = get_commands(block)
    contracts = []
    for instruction in instructions
      if is_contract?(instruction)
        # Is a line with contarct instantiation
        # Ex : contract.requires(:method, :paint).calls(:draw)
        contracts.push(get_contract(instruction))
      elsif is_shortcut?(instruction)
        # Is a line with just the contract to apply 
        # Ex : calls(:draw)
        contracts.push(get_short_contract(instruction))
      end
    end
    return contracts
  end
  
end
