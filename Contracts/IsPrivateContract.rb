=begin
  Contract checking that a method must be private
=end
require_relative "./Contract"

class IsPrivateContract < Contract
  
  @call_names = ["is_private", "are_privates"]
  def apply(args, block)
      class_inst = Object.const_get(@class_name.to_s)
      if class_inst.private_instance_methods.include?(@method_name) or
         class_inst.private_methods.include?(@method_name)
        return true
      else
        return false
      end
  end
  
end