require_relative './PGDBContract'
=begin
  PostgreSQL DB table contains given fields
=end
class HaveFieldContract < PGDBContract
  def get_fields(table_name, db)
    query = "SELECT column_name FROM information_schema.columns WHERE table_name = '#{table_name}';"
    results = db.exec(query)
    fields = Array.new
    for result in results
      fields += [result["column_name"]]
    end
    return fields
  end

  @call_names = ['has_field', 'have_field']

  def apply(args, block)
    db = open_db
    result = true
    table_name = args[0]
    for name in args
      if (name <=> table_name) != 0
        exist = exist_field?(table_name, field_name, connection = nil)
        result = result && exist      end
    end
    close_db(db)
    return result
  end
end

