require_relative './Contract'

=begin
  Contract for database contract checking
  Subclasses must implement 
    openDB
    closeDB
    listTables
    getFields
=end
 
class DBContract < Contract

  def initialize(class_name, method, ucontract, host = '127.0.0.1', user = 'postgres', password = 'root', db_name = 'Magazine', port = 5432)
    super(class_name, method, ucontract)
    @host = "#{host}"
    @user = "#{user}"
    @password = "#{password}"
    @db_name = "#{db_name}"
    @port = port
  end

  # return true if table_name exists in the db
  # create new connection to db if no db arg given
  def exist_table?(table_name, connection = nil)
    if connection == nil
      # Open db
      db = open_db
    else
    db = connection
    end

    # Retrieve list of tables
    tables = list_tables(db)
    # Check if table_name exists
    #exists = tables.include?("#{table_name}") # case sensitive check
    exists = tables.map(&:downcase).include?("#{table_name}".downcase)
    
    if connection == nil
      # Close db
      close_db(db)
    end
    return exists
  end

  # return true if field_name in table_name exists in the db
  # create new connection to db if no db arg given
  def exist_field?(table_name, field_name, connection = nil)
    if connection == nil
      # Open db
      db = open_db
    else
      db = connection
    end
    
    # Check if field exists
    exists = get_fields("#{table_name}", db).include?("#{field_name}")
    
    if connection == nil
      # Close db
      close_db(db)
    end
    return exists
  end

end