=begin
  Contract checking that a given call is always the last execution made
=end
require_relative "./Contract"
require_relative "../InstructionUtils/LastCallSeeker"

class EndWithContract < Contract
  
  @call_names = ["ends_with_call", "end_with_call"]
  def apply(args, block)
    lcs = LastCallSeeker.new(@class_name, @method_name)
    return lcs.apply(args[0])
  end
  
end