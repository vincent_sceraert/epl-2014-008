=begin
  Contract checking that the assignment of a variable
=end
require_relative "./Contract"
require_relative "../InstructionUtils/AssignSeeker"

class AssignContract < Contract
  
  @call_names = ["assign", "assigns"]
  def apply(args, block)
    if @method_name != "@"
      # Assignment done in a given methods
      as = AssignSeeker.new(@class_name, @method_name)
      return as.apply(args[0])
    else
      # Assignment done outside of methods
      class_obj = Object.const_get(@class_name)
      
      if args[0].to_s.start_with?("@@")
        return class_obj.class_variable_defined?(args[0])
      elsif args[0].to_s.start_with?("@")
        return class_obj.instance_variable_defined?(args[0])
      end
      
      #Not a instance or class variable
      return false
    end
  end
  
end
