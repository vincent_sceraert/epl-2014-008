=begin
  Contract checking that a method always makes a return
=end
require_relative "./Contract"
require_relative "../InstructionUtils/MethodCallSeeker"

class HasReturnContract < Contract
  
  @call_names = ["has_return", "have_return"]
  def apply(args, block)
    mcs = MethodCallSeeker.new(@class_name, @method_name)
    return mcs.apply(:return)
  end
  
end
