=begin
  Contract checking that a method is no longer than a given length
=end
require_relative "./Contract"
require_relative "../Parser/parser"

class MaxLengthContract < Contract
  
  @call_names = ["maxLength"]
  def apply(args, block)
    max_length = args[0]
    class_obj = Object.const_get(@class_name)
    src = Parser.get_source_code(class_obj, @method_name)
    # Get the lines
    lines = src.split("\n")
    # Remove empty lines and comment with first character as #
    lines = lines.select{|line| 
      line_strip = line.strip
      line_strip != "" and (line_strip)[0] != "#"
    }
    # Set initial number of lines minus 1 for the "def ..." line
    number_of_lines = lines.length - 1
    
    # Remove comment with =begin ... =end
    comment_lines = Array.new
    index = 0
    while index < number_of_lines + 1
      if lines[index].strip == "=begin"
        first_i = index
        index += 1
        while lines[index].strip != "=end"
          index += 1
        end
        last_i = index
        comment_lines.push([first_i, last_i])
      end
      index += 1
    end
    # Count length of comments block and substract them to the number of line
    for indexes in comment_lines
      number_of_lines -= (indexes[1] - indexes[0]) + 1
    end
    # Decrement if last line contains only end
    number_of_lines = number_of_lines - 1 if lines.last != nil and lines.last.strip == "end"
    return max_length >= number_of_lines ? true : false
  end
  
end