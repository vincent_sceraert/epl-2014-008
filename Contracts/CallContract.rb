=begin
  Contract checking a given call is always performed by a method
=end
require_relative "./Contract"
require_relative "../InstructionUtils/MethodCallSeeker"

class CallContract < Contract
  
  @call_names = ["call", "calls"]
  def apply(args, block)
    mcs = MethodCallSeeker.new(@class_name, @method_name)
    return mcs.apply(args[0])
  end
  
end
