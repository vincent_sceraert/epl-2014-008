=begin
  Contract checking thta a method makes a given static call of a given class
=end
require_relative "./Contract"
require_relative "../InstructionUtils/MethodCallSeeker"

class StaticCallContract < Contract
  
  @call_names = ["static_call", "static_calls"]
  def apply(args, block)
    mcs = MethodCallSeeker.new(@class_name, @method_name)
    return mcs.apply(args[1], args[0])
  end
  
end
