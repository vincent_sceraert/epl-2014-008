=begin
  Contract checking that if a first contract passes, the second must pass
=end
require_relative "./ConditionalContract"

class BidirectionalConditionalContract < ConditionalContract
  
  @call_names = ["has_biconditional_contracts", "have_biconditional_contracts"]
  def apply(args, block)
    instructions = get_commands(block)
    contracts = get_contracts(block)
    
    if contracts.length != 2
      puts "There as an error with the contract \n#{block.source}".red 
      return false
    end
=begin
    return true if !apply_contract(contracts[0])
    return false if !apply_contract(contracts[1])
    return true
=end  
    first_result = apply_contract(contracts[0])
    second_result = apply_contract(contracts[1])
    first_flow = first_result ? second_result : true
    second_flow = second_result ? first_result : true
    return (first_flow and second_flow)
    
  end

end
