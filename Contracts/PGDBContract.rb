
require_relative './DBContract'
require 'pg'
=begin
  Base contract for postgreSQL database with open-close operations
=end
 
class PGDBContract < DBContract

  def open_db
    conn = PG::Connection.new(:dbname => @db_name, :port => @port , :user => @user, :password => @password)
    return conn
  end

  def close_db(db)
    db.close()
  end

end