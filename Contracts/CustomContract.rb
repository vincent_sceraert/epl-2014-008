=begin
  Custom class which takes a block and use it for the verification  
=end
require_relative "./Contract"

class CustomContract < Contract
  
  @call_names = ["respects_custom_contract", "respect_custom_contract"]
  def apply(args, block)
    # Last arg of block is always the ucontract instance
    return block.call(args, @ucontract)
  end
  
end
