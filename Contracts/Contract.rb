=begin
  Base contract
=end
class Contract
  
  # List of calls to match with the class
  @call_names = Array.new()
  def self.call_names
    return @call_names
  end
  
  def initialize(class_name, method, ucontract)
    @class_name = class_name
    @method_name = method
    @ucontract = ucontract
  end
  
end