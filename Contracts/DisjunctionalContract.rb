=begin
  Contract checking that if a first contract passes, the second must pass
=end
require_relative "./CompositeContract"

class DisjunctionalContract < CompositeContract
  
  @call_names = ["has_disjunctional_contracts", "have_disjunctional_contracts"]
  def apply(args, block)
    instructions = get_commands(block)
    contracts = []
    for instruction in instructions
      if is_contract?(instruction)
        # Is a line with contarct instantiation
        # Ex : contract.requires(:method, :paint).calls(:draw)
        contracts.push(get_contract(instruction))
      elsif is_shortcut?(instruction)
        # Is a line with just the contract to apply 
        # Ex : calls(:draw)
        contracts.push(get_short_contract(instruction))
      end
    end
    for contract in contracts
      return true if apply_contract(contract)
    end
    return false
  end
  
end
