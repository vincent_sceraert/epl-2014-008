=begin
  Contract checking a method always begin with a given call as first execution
=end
require_relative "./Contract"
require_relative "../InstructionUtils/FirstCallSeeker"

class BeginWithContract < Contract
  
  @call_names = ["begins_with_call", "begin_with_call"]
  def apply(args, block)
    fcs = FirstCallSeeker.new(@class_name, @method_name)
    return fcs.apply(args[0])
  end
  
end