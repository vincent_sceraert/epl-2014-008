=begin
  Contract checking that a method must be public
=end
require_relative "./Contract"

class IsPublicContract < Contract
  
  @call_names = ["is_public", "are_publics"]
  def apply(args, block)
      class_inst = Object.const_get(@class_name.to_s)
      if class_inst.public_instance_methods.include?(@method_name) or
         class_inst.public_methods.include?(@method_name)
        return true
      else
        return false
      end
  end
  
end