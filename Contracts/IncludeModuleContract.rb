=begin
  Contract checking that the class has a given module included
=end
require_relative "./Contract"
require_relative "../InstructionUtils/AssignSeeker"

class IncludeModuleContract < Contract
  
  @call_names = ["includes_module", "include_module"]
  def apply(args, block)
    class_inst = Object.const_get(@class_name)
    include_module = false
    class_inst.included_modules.each {|mod| 
      include_module = true if mod.class == Module and mod.to_s == args[0].to_s
    }
    return include_module
  end
  
end
