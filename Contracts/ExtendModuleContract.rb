=begin
  Contract checking that the class has a given module included
=end
require_relative "./Contract"
require_relative "../InstructionUtils/AssignSeeker"

class ExtendModuleContract < Contract
  
  @call_names = ["extends_module", "extend_module"]
  def apply(args, block)
    class_inst = Object.const_get(@class_name)
    extend_module = false
    (class << class_inst; self end).included_modules.each {|mod| 
      extend_module = true if mod.class == Module and mod.to_s == args[0].to_s
    }
    return extend_module
  end
  
end
