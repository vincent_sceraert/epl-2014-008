require_relative './PGDBContract'
=begin 
  DB table exists for PostgreSQL DB  
=end
class MatchTableContract < PGDBContract

  def list_tables(db)
    query = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';"
    results = db.exec(query)
    tables = Array.new
    for result in results
      tables += [result["table_name"]]
    end
    return tables
  end
  
  @call_names = ['matches_table', 'match_table']
  def apply(args, block)
    if args.length == 0
      args.push(@class_name)
    end
    db = open_db
    result = true
    for name in args
      exist = exist_table?(name, db)
      result = result && exist
      #puts name.to_s + ": " + exist.to_s + "/" + result.to_s
    end
    close_db(db)
    return result
  end

end



