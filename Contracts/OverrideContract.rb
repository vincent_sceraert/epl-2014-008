=begin
  Contract checking that a class overrides a given method
=end
require_relative "./Contract"
require_relative "../Parser/parser"

class OverrideContract < Contract
  
  @call_names = ["overrides", "override"]
  def apply(args, block)
    method = args[0]
    parent = get_contract_parent(method)   
    # Get the parent's method
    parent_obj = Object.const_get(parent)
    parent_meth = Parser.get_commands(Parser.parse(parent, method))
    # Get the child's method
    child_obj = Object.const_get(@class_name)
    child_meth = Parser.get_commands(Parser.parse(@class_name, method))
    # Compare their parse tree
    return true if child_meth != "" and child_meth != parent_meth
    return false
    
  end

=begin
  Retrieve parent which declared the contract for overriding
=end  
  def get_contract_parent(method)
    child_obj = Object.const_get(@class_name)
    if child_obj.class == Class
      parent = child_obj.superclass
      while parent != BasicObject
        for contract in parent.children_contracts
          if contract[0].target = :children and contract[1] == :override and contract[2] == [:"#{method}"]
            return parent.to_s
          end
        end
        parent = parent.superclass
      end
      return BasicObject.to_s
    end
    return BasicObject.to_s
  end
  
  private :get_contract_parent
  
end
