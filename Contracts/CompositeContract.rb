=begin
  Contract never called, used as parent for contract which takes contracts as arguments
=end
require_relative "./Contract"
require_relative "../UContract"

class CompositeContract < Contract
  
protected  
  def get_commands(block)
    parse_tree = Ripper.sexp(block.source)
    return parse_tree[1][0][2][2]
  end
  
  def is_contract?(instruction)
    return false if instruction[1][0] != :call
    return false if instruction[1][1][1][1][1][1] != "contract"
    return true
  end
  
  def get_contract(instruction)
    type = get_type(instruction)
    type_args = get_type_args(instruction)
    contract = get_contract_name(instruction)
    contract_args = get_contract_args(instruction)
    ucontract = UContract.new(@class_name)
    if type_args.length == 1
      ucontract.send type, type_args[0]
    elsif type_args.length == 2
      ucontract.send type, type_args[0], type_args[1]
    else
      puts "Error in contract \n#{block}\n missing requires or suggets".red
    end
    return [ucontract, contract, contract_args]
  end
  
  def get_type(parse_tree)
    return parse_tree[1][1][1][3][1]
  end
  
  def get_type_args(parse_tree)
    params = []
    params_raw =  parse_tree[1][1][2][1][1]
    first_param = params_raw[0][1][1][1]
    params.push(first_param)
    if params_raw.length > 1
      second_param = params_raw[1][1][1][1] 
      params.push(second_param)
    end
    return params
  end
  
  def get_contract_name(parse_tree)
    return parse_tree[1].last[1].to_s
  end
  
  def get_contract_args(parse_tree)
    params = []
    for param in parse_tree[2][1][1]
      params.push(param[1][1][1])
    end
    return params
  end
  
  def is_shortcut?(instruction)
    return false if instruction[1][0] != :fcall
    return true
  end
  
  def get_short_contract(instruction)
    contract_call = instruction[1][1][1]
    contract_args = get_short_args(instruction)
    new_contract = @ucontract.copy 
    # Change target to avoid that contract is inherited by children
    new_contract.target = :self if new_contract.target == :hierarchy or 
                                   new_contract.target == :subclasses or
                                   new_contract.target == :methodsOverriding or
                                   new_contract.target == :methodAndOverriders or
                                   new_contract.target == :directSubclasses
    return [new_contract, contract_call, contract_args]
  end
  
  def get_short_args(instruction)
    params = []
    for arg in instruction[2][1][1]
      params.push(arg[1][1][1])
    end
    return params
  end
  
  def apply_contract(contract)
    ucontract = contract[0]
    contract_call = contract[1]
    contract_args = contract[2]
    ucontract.send contract_call, contract_args
    result = ucontract.result
    ucontract.remove_contract(ucontract.contract, ucontract.args, ucontract.block)
    return result == :pass
  end
  
end
