=begin
  Contract checking that a class implements a given method
=end
require_relative "./Contract"

class ImplementContract < Contract
  
  @call_names = ["implements", "implement"]
  def apply(args, block)
    class_inst = Object.const_get(@class_name.to_s)
    method_name = args[0]
    # Check all kind of methods
    if class_inst.instance_methods.include?(:"#{method_name}") || 
       class_inst.methods.include?(:"#{method_name}") || 
       class_inst.private_instance_methods.include?(:"#{method_name}") ||
       class_inst.private_methods.include?(:"#{method_name}") ||
       class_inst.protected_instance_methods.include?(:"#{method_name}") ||
       class_inst.protected_methods.include?(:"#{method_name}")
        return true
    else
        return false
    end
  end
  
end