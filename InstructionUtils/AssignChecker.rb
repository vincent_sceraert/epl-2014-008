=begin
  Module which chekc if a given instruction is the wanted assignment  
=end
module AssignChecker
   def check(instruction)
    case instruction[0]
      when :assign, :opassign
        return instruction[1][1][1].to_s == @name.to_s
      else 
        return false
    end
  end
end