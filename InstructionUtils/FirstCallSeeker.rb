=begin
  Check if every possible first instruction of a method is the wanted method call  
=end
require_relative "../Parser/parser"
require_relative "./InstructionChecker"
require_relative "./CallChecker"
require_relative "./FirstSeeker"

class FirstCallSeeker 
  include InstructionChecker
  include CallChecker
  include FirstSeeker
  
  
end
