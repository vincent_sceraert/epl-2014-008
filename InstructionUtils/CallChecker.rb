=begin
  Module which check if a given instruction is the wanted method call  
=end
module CallChecker
  def check(instruction)
    case instruction[0]
      when :command, :vcall, :fcall
        return instruction[1][1].to_s == @name.to_s 
      when :command_call, :call
        return (instruction[1][1][1].to_s == @other_class_name.to_s and instruction[3][1].to_s == @name.to_s)   
      when :method_add_arg
        return check(instruction[1])
      when :super, :zsuper
        return false if :"#{@name}" != :super
        return true
      when :return, :return0
        return false if :"#{@name}" != :return
        return true
      else
        return false
    end
  end
end