=begin
  Look for a path where the wanted property is not respected  
=end
require_relative "../Parser/parser"

module InstructionSeeker
  
  def initialize(class_name, method_name)
    @class_name = class_name
    @method_name = method_name
  end
  
  def apply(name, other_class_name = nil)
    @name = name.to_s
    @other_class_name = other_class_name.to_s
    for inst in Parser.get_commands(Parser.parse(@class_name, @method_name))     
      return true if dispatch_check(inst) == true
    end 
    return false
  end

protected  
  def dispatch_check(inst)
    case inst[0]
      when :if
        return check_if(inst)
      when :else
        return check_else(inst[1])
      when :elsif
        return check_else(inst[2])
      when :void_stmt
        return false
      when :case
        return check_switch(inst[2])
      when :while
        return check_while(inst[2])
      else
        return check(inst)
    end
  end
  
=begin
  Check if if or elsif or else doesn't respect constraint 
=end
  def check_if(commands)
    if_result = enumerate_instructions(commands[2])
    current = commands[3]
    # If no else, we call can never be made
    return false if current == nil
    
    else_result = true
    while current != nil
      # Check elsif and else stmts
      else_result = false if dispatch_check(current) == false
      current = current[3]
    end
    # If and all else and elsif must be good
    return (if_result and else_result)
  end
  
  def check_else(commands)
    enumerate_instructions(commands)
  end
  
  def check_switch(commands)
    current = commands
    results = Array.new
    has_else = false
    
    while current != nil
      if current[0] == :else
        has_else = true
        result = enumerate_instructions(current[1])
        results.push(result)
     elsif current[0] == :when 
        result = enumerate_instructions(current[2])
        results.push(result)
      end
      current = current[3]      
    end
    
    # If no else, call can never be performed
    return false if has_else == false
    
    for result in results
      return false if result == false
    end
    return true
  end
  
  def check_while(commands) 
    return enumerate_instructions(commands)
  end
  
  def enumerate_instructions(commands)
    result = false
    for inst in commands
      # True if any stmt is the call
      result = true if dispatch_check(inst) == true   
    end
    return result
  end
  
end


