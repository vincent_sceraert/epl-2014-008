=begin
  Return every possible first instruction of a given method
=end
module FirstSeeker
  
   def get_instructions(stmts)
    first_stmts = Array.new
    index = 0
    exit = false
    while !exit
      first_stmts.push(stmts[index])
      break if stmts[index] == nil
      case stmts[index][0]
      when :vcall, :command, :method_add_arg
        exit = true
      when :if
        exit = if_has_else(stmts[index])
      when :case
        exit = case_has_else(stmts[index])
      when :while
        exit = false
      else
        exit = true
      end
      index += 1
    end
    return first_stmts
  end
  
  private :get_instructions
end