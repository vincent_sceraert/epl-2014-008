=begin
  Return every possible last instruction of a given method
=end
module LastSeeker
  
   def get_instructions(stmts)
    last_stmts = Array.new
    index = stmts.length - 1
    exit = false
    while !exit and index != -1
      last_stmts.push(stmts[index]) if stmts[index] != nil 
      next if stmts[index] == nil
      case stmts[index][0]
      when :vcall, :command, :method_add_arg
        exit = true
      when :if
        exit = if_has_else(stmts[index])
      when :case
        exit = case_has_else(stmts[index])
      when :while
        exit = false
      else
        exit = true
      end
      index -= 1
    end
    return last_stmts
  end
  
  private :get_instructions
  
end