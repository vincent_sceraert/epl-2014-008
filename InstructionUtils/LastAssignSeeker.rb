=begin
  Check if every possible last instruction of a method is the wanted assignment  
=end
require_relative "./AssignChecker"
require_relative "./LastSeeker"
require_relative "./InstructionChecker"

class LastAssignSeeker
  include AssignChecker
  include LastSeeker
  include InstructionChecker
  
end
