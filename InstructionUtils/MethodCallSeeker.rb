=begin
  Look for a path where the wanted method call is not performed  
=end
require_relative "./InstructionSeeker"
require_relative "./CallChecker"

class MethodCallSeeker 
  include InstructionSeeker
  include CallChecker
  
end
