=begin
  Check every instruction received  
=end
require_relative "../Parser/parser"

module InstructionChecker
  
  def initialize(class_name, method_name)
    @class_name = class_name
    @method_name = method_name
  end
  
  def apply(call_name)
    @name = call_name
    stmts = Parser.get_commands(Parser.parse(@class_name, @method_name))
    relevant_stmts = get_instructions(stmts)
    return check_stmts(relevant_stmts)
  end

protected  
  def check_stmts(stmts)    
    for stmt in stmts
      return false if !check_stmt(stmt)
    end
    return true
  end
  
  def check_stmt(stmt)
      return false if stmt == nil
      case stmt[0]
        when :if
          return check_if(stmt)
        when :else
          return check_stmt(stmt[1][0])
        when :elsif
          return check_stmt(stmt[2][0])
        when :case
          return check_case(stmt)
        when :while
          return check_while(stmt)
        else
          return check(stmt)
      end
  end
  
  def check_if(stmt)
    if_result = check_stmt(stmt[2][0])
    else_result = true
    current = stmt[3]
    while current != nil
      # Check elsif and else stmts
      else_result = false if check_stmt(current) == false
      current = current[3]
    end
    return (if_result and else_result)
  end
  
  def check_case(stmt)
    current = stmt[2]
    results = Array.new
    while current != nil
      if current[0] == :else
        results.push(check_stmt(current[1][0]))
      elsif current[0] == :when 
        results.push(check_stmt(current[2][0]))
      end
      current = current[3]      
    end
    for result in results
      return false if result == false
    end
    return true
  end
  
  def check_while(stmt)
    return check_stmt(stmt[2][0])
  end
  
  def if_has_else(if_stmt)
    current = if_stmt[3]
    has_else = false
    while current != nil
      has_else = true if current[0] == :else
      current = current[3]
    end
    return has_else
  end
  
  def case_has_else(case_stmt)
    current = case_stmt[2]
    has_else = false
    while current != nil
      has_else = true if current[0] == :else
      current = current[3]
    end
    return has_else
  end
  
end