=begin
  Check if every possible first instruction of a method is the wanted assignment  
=end
require_relative "./AssignChecker"
require_relative "./FirstSeeker"
require_relative "./InstructionChecker"

class FirstAssignSeeker
  include AssignChecker
  include FirstSeeker
  include InstructionChecker
  
end
