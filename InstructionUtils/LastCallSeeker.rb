=begin
  Check if every possible last instruction of a method is the wanted method call  
=end
require_relative "../Parser/parser"
require_relative "./InstructionChecker"
require_relative "./CallChecker"
require_relative "./LastSeeker"

class LastCallSeeker 
  include InstructionChecker
  include CallChecker
  include LastSeeker
  
end

