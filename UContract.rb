=begin
  usage contract
=end
require_relative "./Parser/parser"
require_relative "./CustomClasses/String"
require_relative "./CustomClasses/Class"
require_relative "./HTML"
require_relative "./PDF"

class UContract

###############################################################################
###############  STATIC VARIABLES
###############################################################################

	@@contracts_path = File.dirname(__FILE__) + "/Contracts/"

###############################################################################
############### PUBLIC METHODS
###############################################################################
  
  attr_accessor :method_name, :class_name, :target, :result, :contract, :type, :timestamp, :args, :block

	def initialize(class_name)
		@class_name = class_name
		@method_name = nil
		@target = :self
		@type = :violation
		@result = :fail
		@timestamp = nil
		@exceptions = Array.new()
		@@contracts = generate_contracts
		
		# Assigned once the contract is applied
		@contract = nil
		@args = nil
		@block = nil
	end

=begin
  For readability of arguments 
=end
def args_formatter(args)
  args_repr = ""
  for arg in args
    if arg.class == Array and arg.length > 5
      args_repr += arg[0..4].to_s
    elsif arg.class == Hash
      args_repr += arg.first[1].to_s + ", ..."
    else
      args_repr += arg.to_s
    end
    args_repr += ", "
  end
  return args_repr[0..args_repr.length-3]
end

=begin
  Pretty print of the UContract 
=end
def to_s
    repr = "#{@type == :violation ? "Violation" : "Suggestion"} Contract #{@contract}\n"
    repr += "Arguments : #{args_formatter(@args)}\n" if @args != nil and @args.length > 0
    repr += "Block : \n#{@block.source.to_s}" if @block.class == Proc
    repr += "Class : #{@class_name}\n"
    repr += "Method : #{@method_name}\n" if @method_name != "@"
    repr += "Result : #{@result == :pass ? @result.to_s : @result.to_s}\n"
    return repr
  end

=begin
  Returns a copy with every attribute set as same value as the instance 
=end  
  def copy
    new_contract = UContract.new(@class_name)
    # Enumerate all attributes
    for attr in new_contract.instance_variables
      new_contract.instance_variable_set(attr, self.instance_variable_get(attr))
    end
    return new_contract
  end
  
=begin
  Apply the contract with the arguments given 
=end	
	def apply_contract(contract, args, block)
	  puts "There was an error for contract".red +
	  "#{contract.to_s}(#{args.to_s})".yellow +
	  "in class ".red + "#{@class_name.to_s}".yellow + " : " +
	  "make sure that make call requires or suggests".red if @method_name == nil
	  if contract_excepted()
	    # Remove contract from array of class because not relevant
	    remove_contract(contract, args, block)
	  elsif @method_name != nil
	    # Enumerate list of contracts classes from Contracts folder and choose the right one
      for dyn_contract in @@contracts
        class_contract = dyn_contract[1] if (dyn_contract[0].to_s == contract.to_s)
      end
      inside = false
      ObjectSpace.each_object(Class) {|cl| inside = true if class_contract == cl.to_s }
      if !inside
        puts "#{class_contract.to_s} class not found".red
        return false 
      end
      # Create the contract instance
      class_obj = Object.const_get(class_contract)
      obj = class_obj.new(@class_name, @method_name, self)
      # Apply verification
      result = obj.apply(args.flatten, block)
      # Set instance variables
      @result = result ? :pass : :fail
      @timestamp = Time.new
      @contract = contract
      @args = args
      @block = block
    end
     return self
  end

=begin
  Add contract in contracts array if not already in it 
=end  
  def add_contract(contract, args, block)
    class_obj = Object.const_get(@class_name)
    elem = [self, contract, args, block]
    if class_obj.contracts == nil
      class_obj.contracts = [elem]
    elsif !class_obj.contracts.include?(elem)
      class_obj.contracts.push(elem)
    end
  end

=begin
  Remove a contract from the contract array of the targeted class
=end 
  def remove_contract(contract, args, block)
    class_obj = Object.const_get(@class_name)
    elem = [self, contract, args, block]
    class_obj.contracts.delete(elem)
  end

=begin
  Add contract in contracts for children array if not already in it 
=end 
  def add_children_contract(contract, args, block)
    class_obj = Object.const_get(@class_name)
    elem = [self, contract, args, block]
    if class_obj.children_contracts == nil
      class_obj.children_contracts = [elem]
    elsif !class_obj.children_contracts.include?(elem)
      class_obj.children_contracts.push(elem)
    end
  end

###############################################################################
############### DSL METHODS
###############################################################################

	def requires(target, method = nil)
		@type = :violation
		define_target(target, method)
		return self
	end

	def suggests(target, method = nil)
		@type = :suggestion
		define_target(target, method)
		return self
	end

	def exception(target, rule, pattern)
		new_exception = [target, rule, pattern]
		if target != :methods and target != :classes
		  puts "target not valid for rule #{rule} with pattern #{pattern} in class #{@class_name},\n".red + 
		       "must be :methods or :classes".red
		  return self
		end
		if !@exceptions.include?(new_exception)
			@exceptions.push(new_exception)
		end
		return self
	end

###############################################################################
############### PRIVATE METHODS
###############################################################################

private
=begin
  Generate dynamically the list of contract verification classes 
=end
  def generate_contracts
    defined_contracts = {}
    Dir[File.dirname(__FILE__) + '/Contracts/*.rb'].each { 
      |file| 
      # Get file name
      contract_file =  file.split("/").last
      # Get contract name
      contract_name = contract_file.split(".")[0]
      require file
      call_names = Object.const_get(contract_name).call_names
      # Associate call names defined in teh class to the class
      if call_names != nil
        call_names.each {|key| defined_contracts.store(key, contract_name)}
      end
    }
    return defined_contracts
  end

=begin
   Set the target according to the target given
=end
 	def define_target(target, method)
 	  target = :"#{target}" if target.class != Symbol
 	  # Set the @target field
 	  @target = :nil
 	  @target = :self if [:method, :class, :allMethods].include?(target)
 	  @target = :children if [:methodsOverriding, :subclasses, :allSubclassesMethods].include?(target)
 	  @target = :hierarchy if [:methodAndOverriders, :hierarchy, :allHierarchyMethods].include?(target)
 	  @target = :directChildren if :directSubclasses == target

    # Set the @method_name field
 	  @method_name = method if [:method, :methodsOverriding, :methodAndOverriders, :superMethods].include?(target)
 	  @method_name = "@" if [:class, :subclasses, :directSubclasses, :hierarchy].include?(target)
 	  @method_name = "." if [:allMethods, :allSubclassesMethods, :allHierarchyMethods].include?(target)
 	  
 	  # Target not known
 	  if target == :nil
      puts "Target not known for method #{@method_name} : Contract not applied".red 
      @target = :self
      @method_name = ""
    end
 	end
 	
=begin
  Check if the method/class is not in exceptions
=end 
  def contract_excepted
    for exception in @exceptions
      field_to_check = exception[0] == :classes ? @class_name.to_s : @method_name.to_s
      rule = exception[1]
      pattern = exception[2].to_s
      case rule
      when :nameBeginWith
          return true if field_to_check.start_with?(pattern)
      when :nameEndWith
          return true if field_to_check.end_with?(pattern)
      when :called
          return true if :"#{field_to_check}" == :"#{pattern}"
      when :nameContainWord
          return true if field_to_check.include?(pattern)
      when :nameDoNotContainWord
          return true if !field_to_check.include?(pattern)
      when :andChildrenOf
          superclasses = []
          parent = self.superclass
          while parent != nil
            superclasses.push(parent.to_s)
            parent = parent.superclass
          end
          return true if superclasses.include?(pattern)
      else
        puts "Exception #{rule.to_s} unknown in class #{@class_name}".red
        return true
      end
    end
    return false
  end

=begin
  Check if the contract given is declared in the dynamically constructed array 
=end
 	def contract_exists?(contract)
 	  for defined_contract in @@contracts.keys
 	    return true if (contract.to_s == defined_contract.to_s)
 	  end
 		return false
 	end

=begin
  Check if the method is already add to the class definition 
=end
 	def method_exists?
 		class_obj = Object.const_get(@class_name)
 		if class_obj.methods.include?(:"#{@method_name}") or
 		   class_obj.instance_methods.include?(:"#{@method_name}") or
 		   class_obj.private_methods.include?(:"#{@method_name}") or
 		   class_obj.private_instance_methods.include?(:"#{@method_name}") or
 		   class_obj.protected_methods.include?(:"#{@method_name}") or
 		   class_obj.protected_instance_methods.include?(:"#{@method_name}") or
 		   class_obj.public_methods.include?(:"#{@method_name}") or
 		   class_obj.public_instance_methods.include?(:"#{@method_name}") 
 		   return true
 		end
 		return false
 	end

###############################################################################
############### HOOKS METHODS
###############################################################################

public
=begin
  Method called when a unknown method is called on the class 
=end
	def method_missing(method, *args, &block)
		if contract_exists?(method)
			add_contract(method, args, block) if @target == :self or @target == :hierarchy
			add_children_contract(method, args, block) if @target == :children or @target == :directChildren or @target == :hierarchy
			if (method_exists? or @method_name == "@") and (@target == :self or @target == :hierarchy)
				return apply_contract(method, args, block)
			elsif @method_name == "."
				#TODO Allow declaration of allMethods anywhere in the code
			end
		else
			puts "Contract #{method} in class #{@class_name} is unknown".red
			@method_name = ""
		end
		return self
	end

end
