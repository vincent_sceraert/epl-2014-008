require 'erb'
=begin
  Class which allow us to create a html page from a list of contracts for easy visualization  
=end
class UContractHTML
  def initialize(contracts)
    @timestamp = Time.new
    @contracts = contracts
  end

  def get_binding
    binding
  end

  @sort_by_fields = [:class, :method, :contract, :type, :result]

  def self.get_list_contracts(classes, contracts, results, types, sort_by)
    # create local variables to avoid side effects
    p_sort_by = :class if @sort_by_fields.include?(sort_by)
    p_classes = classes.class != Array && classes != :all ? [classes] : classes
    p_contracts = contracts.class != Array && contracts != :all ? [contracts] : contracts
    p_types = types.class != Array && types != :all ? [types] : types
    p_results = results.class != Array && results != :all ? [results] : results
    contracts_list = get_env_contracts
    # Check classes
    if p_classes != :all
      contracts_list.select!{
      |contract|
        p_classes.include?(:"#{contract.class_name}")
      }
    end
    # Check contracts
    if p_contracts != :all
      contracts_list.select!{
      |contract|
        p_contracts.include?(:"#{contract.contract}")
      }
    end
    # Check results
    if p_results != :all
      contracts_list.select!{
      |contract|
        p_results.include?(:"#{contract.result}")
      }
    end
    # Check types
    if p_types != :all
      contracts_list.select!{
      |contract|
        p_types.include?(:"#{contract.type}")
      }
    end
    # Check sort field
    contracts_list.sort!{
      |c1, c2|
      case p_sort_by
      when :class
        c1.class_name <=> c2.class_name
      when :method
        c1.method_name <=> c2.method_name
      when :contract
        c1.contract <=> c2.contract
      when :type
        c1.type <=> c2.type 
       when :result
        c1.result <=> c2.result 
      end
      
    }
    return contracts_list
  end 

  def self.show_contracts(classes: :all, contracts: :all, results: :all, types: :all, sort_by: :class)   
    contracts_list = get_list_contracts(classes, contracts, results, types, sort_by)
    print_contracts(contracts_list)
  end

  def self.get_env_contracts
    contracts = Array.new
    ObjectSpace.each_object(Class) {
    |cl|
      cl.get_contracts.each {
      |contract|
        contracts.push(contract)
      }
    }
    return contracts
  end

  def self.generate(contracts)
    @path = "result/"
    result_html =ERB.new(File.read(@path+"result.html.erb"))
    file = File.open(@path+"result.html", "w")
    file.write(result_html.result(UContractHTML.new(contracts).get_binding))
    file.close
  end

  def self.print_contracts(contracts)
    generate(contracts)
    puts "#{File.dirname(__FILE__)}/"+@path+"result.html"
    system("explorer file:///#{File.dirname(__FILE__)}/"+@path+"result.html")
  end

end

